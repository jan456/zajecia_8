//
// Created by Monika on 2020-05-09.
//
#include <stdio.h>


void Test(FILE *file, float *x, float *y, float *rho){
    fseek(file, 16, SEEK_SET);

    for(int i=0; i<9; i++)
    {
        fscanf(file, "%f", &x[i]);
        printf("%.5f\t",x[i]);

        fseek(file, 2,  SEEK_CUR);
        fscanf(file,"%f", &y[i]);
        printf("%.5f\t",y[i]);

        fseek(file, 1,SEEK_CUR);
        fscanf(file, "%f", &rho[i]);
        printf("%.3f\n",rho[i]);

        fseek(file, 4, SEEK_CUR);
    }

    fseek(file, 1, SEEK_CUR);

    for(int i=9; i<50; i++)
    {
        fscanf(file, "%f", &x[i]);
        printf("%.5f\t", x[i]);

        fseek(file, 2, SEEK_CUR);
        fscanf(file,"%f", &y[i]);
        printf("%.5f\t",y[i]);

        fseek(file, 1,SEEK_CUR);
        fscanf(file, "%f", &rho[i]);
        printf("%.3f\n",rho[i]);

        fseek(file, 5, SEEK_CUR);
    }
    return;
}