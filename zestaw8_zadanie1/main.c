#include <stdio.h>
#include <stdlib.h>
#include "projekt/glowa.h"

int main()
{
    FILE *file;
    file=fopen("P0001_attr.txt","r+w");

    if(file == NULL){
        printf("null");
        exit(1);
    }
    else
    {
        float *x;
        x = (float *)calloc(50, sizeof(float));
        float *y ;
        y = (float *)calloc(50, sizeof(float));
        float *rho;
        rho= (float *)calloc(50, sizeof(float));

         Test(file, x, y, rho);

         printf("\n\nDane dla kolumny X");

         fprintf(file, "\nSREDNIA\t\tMEDIANA\t\tODCH.ST\n");

         float mean_x = Mean(x, 50);
         float median_x = Mediana(x, 50);
         float sd_x = SD(x,  50);

         fprintf(file, "%f\t%f\t%f\n", mean_x, median_x, sd_x);

         printf("\n\nDane dla kolumny Y");
         float mean_y = Mean(y,  50);
         float median_y = Mediana(y, 50);
         float sd_y = SD(y, 50);

         fprintf(file, "%f\t%f\t%f\n", mean_y, median_y, sd_y);

         printf("\n\nDane dla kolumny RHO");
         float mean_rho = Mean(rho, 50);
         float median_rho = Mediana(rho, 50);
         float sd_rho = SD(rho,  50);

         fprintf(file, "%f\t%f\t%f\n", mean_rho, median_rho, sd_rho);

    free(x);
    free(y);
    free(rho);

    fclose(file);

    }

    return 0;
}

