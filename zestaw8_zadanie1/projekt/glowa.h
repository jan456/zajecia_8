//
// Created by Monika on 2020-05-09.
//

#ifndef ZAJECIA_8_GLOWA_H
#define ZAJECIA_8_GLOWA_H

void Test(FILE *file, float *x, float *y, float *rho);
float Mean(float *x, int size);
float SD(float *data, int size);
float Mediana(float *x, int size);

#endif ZAJECIA_8_GLOWA_H
