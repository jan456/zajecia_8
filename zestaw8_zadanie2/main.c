#include <stdio.h>
#include <stdlib.h>
#include <math.h>

float Mean(float *x, int size)
{
    float mean = 0.0;

    for(int i=0 ; i<size; i++)
        mean+=x[i];

    mean=mean/size;
    printf("\nSREDNIA=%f", mean);
    return mean;
}


float Mediana(float *x, int size)
{
    float temp=0.0;
    for (int i = 0; i < 50; i++) {
        for (int j = 0; j < 50 - i; j++) {
            if (x[j - 1] > x[j]) {
                temp=x[j] ;
                x[j] = x[j - 1];
                x[j - 1] = temp;
            }
        }
    }

    int half= size/2;
    float median = (x[half]+x[half-1])/2;
    printf("\nMEDIANA = %f", median);

    return median;
}
float SD(float *data, int size)
{
    float sum = 0.0;
    float mean = 0.0;
    float SD = 0.0;

    int i;
    for (i = 0; i < size; ++i)
    {
        sum += data[i];
    }

    mean = sum/size;

    for(i=0; i<size; i++)
    {
        SD += pow(data[i] - mean, 2);
    }

    SD=sqrt(SD / size);
    printf("\nODCHYLENIE STANDARDOWE= %f", SD);



    return SD;
}

struct statistics
{
    float x[50];
    float y[50];
    float rho[50];
    float mean[3];
    float mediana[3];
    float sd[3];
};

int main()
{
    FILE *file;
    file=fopen("P0001_attr.txt","r+");

    if(file == NULL)
    {
        printf("null");
        exit(1);
    }
    else
    {
        struct statistics xyrho;

        fseek(file, 13, SEEK_SET);

        for(int i=0; i<50; i++)
        {
            fseek(file, 3, SEEK_CUR);
            fscanf(file, "%f \t%f\t%f\n", &xyrho.x[i], &xyrho.y[i], &xyrho.rho[i]);
            printf("%.5f \t%.5f\t%.3f\n", xyrho.x[i], xyrho.y[i], xyrho.rho[i]);
        }

        xyrho.mean[0] = Mean(xyrho.x, 50);
        xyrho.mediana[0] = Mediana(xyrho.x, 50);
        xyrho.sd[0] =  SD(xyrho.x,  50);

        xyrho.mean[1] = Mean(xyrho.y, 50);
        xyrho.mediana[1] = Mediana(xyrho.y, 50);
        xyrho.sd[1] = SD(xyrho.y, 50);

        xyrho.mean[2] = Mean(xyrho.rho, 50);
        xyrho.mediana[2] = Mediana(xyrho.rho, 50);
        xyrho.sd[2] = SD(xyrho.rho, 50);

        long position = ftell(file);

        if(position == EOF)
        {
            for(int i=0; i<3; i++)
            {
                fprintf(file, "\nSREDNIA\tMEDIANA\tODCH.STANDARDOWE\n%f\t%f\t%f", xyrho.mean[i], xyrho.mediana[i], xyrho.sd[i]);
            }
        }
        else
        {
            printf("\nPlik juz zostal nadpisany");
        }

        fclose(file);

    }

    return 0;

}
