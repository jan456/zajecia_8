//
// Created by Monika on 2020-05-09.
//

#include <stdio.h>
#include <math.h>

float SD(float *data, int size) {
    float sum = 0.0;
    float mean;
    float SD = 0.0;

    int i;
    for (i = 0; i < size; ++i) {
        sum += data[i];
    }
    mean = sum / size;
    for (i = 0; i < size; ++i)
        SD += pow(data[i] - mean, 2);
    SD=sqrt(SD / size);
    printf("\nODCHYLENIE STANDARDOWE= %f", SD);

    return sqrt(SD / size);
}



