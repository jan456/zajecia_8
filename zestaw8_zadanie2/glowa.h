//
// Created by Monika on 2020-05-12.
//

#ifndef MAIN_2ZADANIE_C_GLOWA_H
#define MAIN_2ZADANIE_C_GLOWA_H


float Mean(float *x, int size);
float Mediana(float *x, int size);
float SD(float *data, int size);

#endif //MAIN_2ZADANIE_C_GLOWA_H
